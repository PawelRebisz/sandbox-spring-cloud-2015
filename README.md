Based on presentation http://www.infoq.com/presentations/spring-cloud-2015

- config server, http://localhost:8888/master/reservation-service
- eureka server, http://localhost:8761
- hystrix dashboard, http://localhost:8060/hystrix
- reservation service, http://localhost:8030/reservations
- reservation client, http://localhost:8050/reservations/names
- zipkin is taken out of the project
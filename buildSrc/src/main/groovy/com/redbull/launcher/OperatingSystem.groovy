package com.redbull.launcher

import org.gradle.api.GradleException
import org.jvnet.winp.NotWindowsException
import org.jvnet.winp.WinProcess

import java.lang.reflect.Field

public class OperatingSystem {

    public OperatingSystemActions get() {
        if (isWindows()) {
            return new Windows()
        } else if (isUnix()) {
            return new Unix()
        }
        throw new GradleException("Frankly failed to determine type of system '${osName()}' that you are using.")
    }

    private static boolean isWindows() {
        osName().contains("win")
    }

    private static boolean isUnix() {
        osName().contains("nix") || osName().contains("nux") || osName().contains("aix")
    }

    private static String osName() {
        System.getProperty("os.name").toLowerCase()
    }

    class Windows implements OperatingSystemActions {
        @Override
        int discoverPid(Process process) {
            try {
                WinProcess winProcess = new WinProcess(process)
                return winProcess.getPid()
            } catch (NotWindowsException exception) {
                // no op
            }
            FAILED_TO_RETRIEVE_PID
        }

        @Override
        void kill(int pid) {
            ProcessBuilder builder = new ProcessBuilder('taskkill', '/F', '/PID', "${pid}")
            Process process = builder.start()
        }
    }

    class Unix implements OperatingSystemActions {
        @Override
        int discoverPid(Process process) {
            try {
                Field pidField = process.getClass().getDeclaredField("pid")
                pidField.setAccessible(true)
                return pidField.getInt(process)
            } catch (Exception exception) {
                // no op
            }
            FAILED_TO_RETRIEVE_PID
        }

        @Override
        void kill(int pid) {
            ProcessBuilder builder = new ProcessBuilder('kill', '-9', "${pid}")
            Process process = builder.start()
        }
    }

    interface OperatingSystemActions {
        int FAILED_TO_RETRIEVE_PID = -1

        int discoverPid(Process process)

        void kill(int pid)
    }
}

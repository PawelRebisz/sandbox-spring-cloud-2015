package com.redbull.launcher

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.TaskAction

import java.nio.file.Path
import java.nio.file.Paths

public class ParallelProcessLauncher extends DefaultTask {

    Path outputDirectory = Paths.get('.')
    Path errorDirectory = Paths.get('.')
    Path pidDirectory = Paths.get('.')

    String processName = "unknown"

    String outputFilename
    String errorFilename
    String pidFilename

    String[] arguments = []

    int waitFor = 0

    @TaskAction
    public void spawnProcess() {
        if (arguments.size() == 0) {
            throw new GradleException('No arguments specified for spawning separate process.')
        }
        println "Service ${processName} is starting."

        ProcessBuilder builder = new ProcessBuilder(arguments)
        builder.redirectOutput(determineOutput(outputDirectory, outputFilename, 'output').toFile())
        builder.redirectError(determineOutput(errorDirectory, errorFilename, 'error').toFile())

        Process process = builder.start()
        int pid = getOs().discoverPid(process)
        storeIntoFile(pid)
        println "Service ${processName} started with pid ${pid} (file storing pid is '${getPidFile().toAbsolutePath()}')."
        if (waitFor > 0) {
            println "Waiting for ${waitFor} seconds on ${processName}..."
            sleep(waitFor * 1000)
        }
    }

    public void terminate() {
        getOs().kill(getPid())
        println "Signaled process with pid '${getPid()}' to shutdown."
    }

    private void storeIntoFile(int pid) {
        getPidFile().toFile().withWriter { out ->
            out.println pid
        }
    }

    private Path determineOutput(Path directory, String filename, String filenameSuffix) {
        directory.resolve(filename ? filename : "${processName}-${filenameSuffix}.log")
    }

    private Path getPidFile() {
        String filename = pidFilename ? pidFilename : "${processName}.pid"
        pidDirectory.resolve(filename)
    }

    private int getPid() {
        if (!getPidFile()) {
            throw new GradleException("No pid file found for '${processName}'.")
        }
        int pid = getPidFile().toFile().text as int
        if (pid < 1) {
            throw new GradleException("Pid from file '${getPidFile()}' has invalid value '${pid}'.")
        }
        pid
    }

    private OperatingSystem.OperatingSystemActions getOs() {
        new OperatingSystem().get()
    }
}